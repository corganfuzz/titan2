ActiveAdmin.register Appointment do
  form do |f|
    f.inputs do
      f.input :order
     # f.input :user
      f.input :appointmentdate
    end
    f.actions
  end

  index do
    selectable_column
    id_column
    column("Order", :order) #, :sortable => :order_id)
    column("Appointment Date", :appointmentdate)
    column("Booked on", :created_at)
    column :updated_at
    actions
  end

  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
   permit_params :order_id, :user_id, :appointmentdate
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end


end
